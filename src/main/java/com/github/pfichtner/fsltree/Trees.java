package com.github.pfichtner.fsltree;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import com.github.pfichtner.fsltree.functional.Function;
import com.github.pfichtner.fsltree.impl.DefaultDirectory;
import com.github.pfichtner.fsltree.impl.DefaultFile;
import com.github.pfichtner.fsltree.impl.SortedDirectory;

/**
 * Helper class holding static methods for creating/maintaining/manipulating
 * trees.
 * 
 * @author Peter Fichtner
 */
public final class Trees {

	private static final Function<String, List<String>> defaultPathSplitter = new Function<String, List<String>>() {
		@Override
		public List<String> apply(String in) {
			return Arrays.asList(in.replaceFirst("^/", "")
					.replaceAll("\\/\\/", "/").split("/"));
		}
	};

	private Trees() {
		super();
	}

	public static Directory newTree(Iterable<String> elements) {
		return newTree(elements, defaultPathSplitter());
	}

	public static Directory newTree(Iterable<String> elements,
			Function<String, List<String>> path) {
		return addTo(elements, path, DefaultDirectory.newRoot());
	}

	public static Directory newSortedTree(Iterable<String> elements) {
		return newSortedTree(elements, defaultPathSplitter());
	}

	public static Directory newSortedTree(Iterable<String> elements,
			Function<String, List<String>> path) {
		return addTo(elements, path, SortedDirectory.newRoot());
	}

	public static Directory addTo(Iterable<String> elements,
			Function<String, List<String>> path, Directory appendTo) {
		for (String element : elements) {
			List<String> pathElements = path.apply(element);
			Directory directory = getOrCreate(appendTo, pathElements);
			new DefaultFile(pathElements.get(pathElements.size() - 1),
					directory);
		}
		return appendTo;
	}

	public static Directory getOrCreate(Directory root, List<String> path) {
		Directory actual = root;
		for (String pathElement : path) {
			Component component = actual.list(pathElement);
			if (component != null) {
				if (!(component instanceof Directory)) {
					throw new IllegalStateException(pathElement
							+ " already present but not a directory ("
							+ component + ")");
				}
				actual = (Directory) component;
			} else {
				actual = actual instanceof SortedDirectory ? new SortedDirectory(
						pathElement, actual) : new DefaultDirectory(
						pathElement, actual);
			}

		}
		return actual;
	}

	/**
	 * Returns all parents of the passed component beginning with the root
	 * element.
	 * 
	 * @param component
	 *            the component to retrieve the parents from
	 * @return all parents of the passed component
	 */
	public static List<Directory> collectParents(Component component) {
		List<Directory> parents = new ArrayList<Directory>();
		Directory actual = component.getParent();
		while (actual != null) {
			parents.add(actual);
			actual = actual.getParent();
		}
		Collections.reverse(parents);
		return parents;
	}

	/**
	 * Gets a Function that transforms a String holding a path (e.g.
	 * <code>"a/foo/bar"</code>) into a List containing the path elements ("a ",
	 * "foo" , "bar" for the example).
	 * 
	 * @return Function that transforms a String holding a path into a List
	 *         containing the path elements
	 */
	public static Function<String, List<String>> defaultPathSplitter() {
		return defaultPathSplitter;
	}

}
