package com.github.pfichtner.fsltree;

import static com.github.pfichtner.fsltree.Trees.defaultPathSplitter;
import static com.github.pfichtner.fsltree.Trees.getOrCreate;
import static com.github.pfichtner.fsltree.functional.Functions.compose;

import java.util.List;

import com.github.pfichtner.fsltree.functional.Function;
import com.github.pfichtner.fsltree.impl.DefaultDirectory;
import com.github.pfichtner.fsltree.impl.DefaultFile;
import com.github.pfichtner.fsltree.impl.SortedDirectory;

// TODO TreeBuilder should not return Directories but objects holding and Directory attribute
public final class TreeBuilder<T> {

	private final Function<T, List<String>> pathSplitter;
	private boolean sorted;

	private TreeBuilder(Function<T, List<String>> pathSplitter) {
		this.pathSplitter = pathSplitter;
	}

	/**
	 * Creates a new TreeBuilder using the passed function to access the path
	 * for each tree element.
	 * 
	 * @param pathGetter
	 *            function to retrieve the path for an element
	 * @return new TreeBuilder instance
	 */
	public static <T> TreeBuilder<T> createFromPath(
			Function<T, String> pathGetter) {
		return createFromPathElements(compose(defaultPathSplitter(), pathGetter));
	}

	/**
	 * Creates a new TreeBuilder using the passed function to access the path's
	 * elements for each tree element.
	 * 
	 * @param pathSplitter
	 *            function to retrieve the path's elements for an tree element
	 * @return new TreeBuilder instance
	 */
	public static <T> TreeBuilder<T> createFromPathElements(
			Function<T, List<String>> pathSplitter) {
		return new TreeBuilder<T>(pathSplitter);
	}

	/**
	 * Flag which type of tree should be created (sorted or unsorted). Defaults
	 * to <code>false</code> (unsorted).
	 * 
	 * @param sorted
	 *            flag whether to create sorted or unsorted trees. In sorted
	 *            trees each directory contains the elements in sorted order.
	 * @return the builder instance
	 */
	public TreeBuilder<T> sorted(boolean sorted) {
		this.sorted = sorted;
		return this;
	}

	/**
	 * Creates a new tree from the passed elements.
	 * 
	 * @param elements
	 *            the elements to create the tree from
	 * @return new tree containing the passed elements
	 */
	public Directory create(Iterable<T> elements) {
		return newTree(elements, this.pathSplitter);
	}

	// ---------------------------------------------------------------------------------------

	private Directory newTree(Iterable<T> elements,
			Function<T, List<String>> path) {
		return addTo(elements, path, createRoot());
	}

	private Directory createRoot() {
		return this.sorted ? SortedDirectory.newRoot() : DefaultDirectory
				.newRoot();
	}

	private static <T> Directory addTo(Iterable<T> elements,
			Function<T, List<String>> path, Directory appendTo) {
		for (T element : elements) {
			List<String> pathElements = path.apply(element);
			Directory directory = getOrCreate(appendTo,
					pathElements.subList(0, pathElements.size() - 1));
			new DefaultFile(pathElements.get(pathElements.size() - 1),
					directory).setContent(element);
		}
		return appendTo;
	}

}
