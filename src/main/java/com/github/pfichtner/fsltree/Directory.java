package com.github.pfichtner.fsltree;

import java.util.List;

public interface Directory extends Component {

	void add(Component component);

	List<Component> list();

	/**
	 * Returns a component in this directory with the passed name or
	 * <code>null</code> if no element with that name exists.
	 * 
	 * @param search
	 *            the component's name to search
	 * @return component in this directory with the passed name or
	 *         <code>null</code>
	 */
	Component list(String search);

	Component list(String... search);

}