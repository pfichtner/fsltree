package com.github.pfichtner.fsltree.impl;

import static com.github.pfichtner.fsltree.Trees.collectParents;
import static com.github.pfichtner.fsltree.functional.Iterables.find;
import static com.github.pfichtner.fsltree.functional.Iterables.transform;
import static com.github.pfichtner.fsltree.functional.Predicates.compose;
import static com.github.pfichtner.fsltree.functional.Predicates.equalTo;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import com.github.pfichtner.fsltree.Component;
import com.github.pfichtner.fsltree.Directory;
import com.github.pfichtner.fsltree.util.Joiner;
import com.github.pfichtner.fsltree.visitor.Visitor;

public class BaseDirectory implements Directory, Comparable<Directory> {

	private final String name;
	private final Directory parent;
	protected final List<Component> components = new ArrayList<Component>();
	private final List<Component> componentsView = Collections
			.unmodifiableList(this.components);

	protected BaseDirectory() {
		this.name = "";
		this.parent = null;
	}

	public BaseDirectory(String name) {
		this(name, null);
	}

	public BaseDirectory(String name, Directory parent) {
		if (name == null || name.length() < 1) {
			throw new IllegalStateException(
					"Name must be not null nor length 0");
		}
		this.name = name;
		this.parent = parent;
		if (parent != null) {
			parent.add(this);
		}
	}

	@Override
	public Directory getParent() {
		return this.parent;
	}

	@Override
	public String getName() {
		return this.name;
	}

	public void add(Component component) {
		this.components.add(component);
	}

	@Override
	public List<Component> list() {
		return this.componentsView;
	}

	@Override
	public Component list(final String search) {
		return find(this.components,
				compose(equalTo(search), Component.getName));
	}

	@Override
	public Component list(String... dirs) {
		if (dirs.length == 0) {
			return this;
		}
		List<String> search = Arrays.asList(dirs);
		Component component = list(search.get(0));
		return component instanceof Directory ? ((Directory) component)
				.list(search.subList(1, search.size()).toArray(new String[0]))
				: component;
	}

	@Override
	public String getAbsolutePath() {
		return Joiner.on('/').join(
				transform(collectParents(this), Component.getName))
				+ '/' + getName();
	}

	@Override
	public void accept(Visitor visitor) {
		visitor.visit(this);
	}

	@Override
	public int compareTo(Directory o) {
		return getName().compareTo(o.getName());
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((this.name == null) ? 0 : this.name.hashCode());
		result = prime * result
				+ ((this.parent == null) ? 0 : this.parent.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof BaseDirectory))
			return false;
		BaseDirectory other = (BaseDirectory) obj;
		if (this.name == null) {
			if (other.name != null)
				return false;
		} else if (!this.name.equals(other.name))
			return false;
		if (this.parent == null) {
			if (other.parent != null)
				return false;
		} else if (!this.parent.equals(other.parent))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "BaseDirectory [name=" + this.name + ", parent=" + this.parent
				+ "]";
	}

}
