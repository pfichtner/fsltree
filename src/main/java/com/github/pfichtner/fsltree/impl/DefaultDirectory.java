package com.github.pfichtner.fsltree.impl;

import com.github.pfichtner.fsltree.Directory;

public class DefaultDirectory extends BaseDirectory {

	public static DefaultDirectory newRoot() {
		return new DefaultDirectory();
	}

	public DefaultDirectory(String name) {
		super(name);
	}

	public DefaultDirectory(String name, Directory parent) {
		super(name, parent);
	}

	private DefaultDirectory() {
		super();
	}

}
