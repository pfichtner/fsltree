package com.github.pfichtner.fsltree.impl;

import com.github.pfichtner.fsltree.Component;
import com.github.pfichtner.fsltree.Directory;
import com.github.pfichtner.fsltree.File;
import com.github.pfichtner.fsltree.Trees;
import com.github.pfichtner.fsltree.functional.Iterables;
import com.github.pfichtner.fsltree.util.Joiner;
import com.github.pfichtner.fsltree.visitor.Visitor;

public class DefaultFile implements File, Comparable<File> {

	private final String name;
	private final Directory directory;
	private Object content;

	public DefaultFile(String name, Directory directory) {
		this.name = name;
		this.directory = directory;
		this.directory.add(this);
	}

	@Override
	public String getName() {
		return this.name;
	}

	@Override
	public Directory getParent() {
		return this.directory;
	}

	@Override
	public String getAbsolutePath() {
		return Joiner.on('/').join(
				Iterables.transform(Trees.collectParents(this),
						Component.getName));
	}

	@Override
	public Object getContent() {
		return this.content;
	}

	public void setContent(Object content) {
		this.content = content;
	}

	@Override
	public void accept(Visitor visitor) {
		visitor.visit(this);
	}

	@Override
	public int compareTo(File o) {
		return getName().compareTo(o.getName());
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((this.directory == null) ? 0 : this.directory.hashCode());
		result = prime * result
				+ ((this.name == null) ? 0 : this.name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DefaultFile other = (DefaultFile) obj;
		if (this.directory == null) {
			if (other.directory != null)
				return false;
		} else if (!this.directory.equals(other.directory))
			return false;
		if (this.name == null) {
			if (other.name != null)
				return false;
		} else if (!this.name.equals(other.name))
			return false;
		return true;
	}

}
