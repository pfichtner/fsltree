package com.github.pfichtner.fsltree.impl;

import static java.util.Collections.binarySearch;

import java.util.Collections;
import java.util.Comparator;

import com.github.pfichtner.fsltree.Component;
import com.github.pfichtner.fsltree.Directory;

public class SortedDirectory extends BaseDirectory {

	private static final Comparator<Object> natural = Collections
			.reverseOrder(Collections.reverseOrder());

	public static SortedDirectory newRoot() {
		return new SortedDirectory();
	}

	public SortedDirectory(String name) {
		super(name);
	}

	public SortedDirectory(String name, Directory parent) {
		super(name, parent);
	}

	private SortedDirectory() {
		super();
	}

	@Override
	public void add(Component component) {
		int idx = binarySearch(this.components, component, natural);
		this.components.add(idx >= 0 ? idx : -idx - 1, component);
	}

	@Override
	public int compareTo(Directory o) {
		return getName().compareTo(o.getName());
	}

}
