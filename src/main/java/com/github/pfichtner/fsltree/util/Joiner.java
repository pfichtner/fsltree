package com.github.pfichtner.fsltree.util;

public class Joiner {

	private final char c;

	private Joiner(char c) {
		this.c = c;
	}

	public static Joiner on(char c) {
		return new Joiner(c);
	}

	public String join(Iterable<String> iterable) {
		StringBuilder sb = new StringBuilder();
		for (String string : iterable) {
			sb.append(string).append(this.c);
		}
		return sb.length() == 0 ? "" : sb.substring(0, sb.length() - 1);
	}

}
