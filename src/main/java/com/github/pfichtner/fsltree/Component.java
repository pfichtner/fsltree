package com.github.pfichtner.fsltree;

import com.github.pfichtner.fsltree.functional.Function;
import com.github.pfichtner.fsltree.visitor.Visitable;

public interface Component extends Visitable {

	public static final Function<Component, String> getName = new Function<Component, String>() {
		@Override
		public String apply(Component component) {
			return component.getName();
		}
	};

	public static final Function<Component, String> getAbsolutePath = new Function<Component, String>() {
		@Override
		public String apply(Component component) {
			return component.getAbsolutePath();
		}
	};

	Directory getParent();

	String getName();

	String getAbsolutePath();

}
