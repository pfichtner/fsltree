package com.github.pfichtner.fsltree;

public interface File extends Component {

	Object getContent();

}