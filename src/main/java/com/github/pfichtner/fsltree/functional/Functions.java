package com.github.pfichtner.fsltree.functional;

public final class Functions {

	private static class FunctionComposition<A, B, C> implements Function<A, C> {
		private final Function<B, C> g;
		private final Function<A, ? extends B> f;

		public FunctionComposition(Function<B, C> g, Function<A, ? extends B> f) {
			this.g = g;
			this.f = f;
		}

		@Override
		public C apply(A a) {
			return this.g.apply(this.f.apply(a));
		}

	}

	private Functions() {
		super();
	}

	public static <A, B, C> Function<A, C> compose(Function<B, C> g,
			Function<A, ? extends B> f) {
		return new FunctionComposition<A, B, C>(g, f);
	}

}
