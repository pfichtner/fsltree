package com.github.pfichtner.fsltree.functional;

public interface Predicate<T> {

	boolean apply(T t);

}