package com.github.pfichtner.fsltree.functional;

public interface Function<T, S> {

	S apply(T t);

}