package com.github.pfichtner.fsltree.functional;

public final class Predicates {

	enum ObjectPredicate implements Predicate<Object> {
		ALWAYS_TRUE {
			@Override
			public boolean apply(Object o) {
				return true;
			}
		},
		ALWAYS_FALSE {
			@Override
			public boolean apply(Object o) {
				return false;
			}
		},
		IS_NULL {
			@Override
			public boolean apply(Object o) {
				return o == null;
			}
		},
		NOT_NULL {
			@Override
			public boolean apply(Object o) {
				return o != null;
			}
		};

		@SuppressWarnings("unchecked")
		<T> Predicate<T> withNarrowedType() {
			return (Predicate<T>) this;
		}
	}

	private Predicates() {
		super();
	}

	private static class IsEqualToPredicate<T> implements Predicate<T> {
		private final T target;

		IsEqualToPredicate(T target) {
			this.target = target;
		}

		@Override
		public boolean apply(T t) {
			return this.target.equals(t);
		}
	}

	private static class CompositionPredicate<A, B> implements Predicate<A> {
		final Predicate<B> p;
		final Function<A, ? extends B> f;

		CompositionPredicate(Predicate<B> p, Function<A, ? extends B> f) {
			this.p = p;
			this.f = f;
		}

		@Override
		public boolean apply(A a) {
			return this.p.apply(this.f.apply(a));
		}

	}

	public static <T> Predicate<T> equalTo(T target) {
		return new IsEqualToPredicate<T>(target);
	}

	public static <A, B> Predicate<A> compose(Predicate<B> predicate,
			Function<A, ? extends B> function) {
		return new CompositionPredicate<A, B>(predicate, function);
	}

	public static <T> Predicate<T> alwaysTrue() {
		return ObjectPredicate.ALWAYS_TRUE.withNarrowedType();
	}

	public static Predicate<String> contains(final String string) {
		return new Predicate<String>() {
			@Override
			public boolean apply(String in) {
				return in.contains(string);
			}
		};
	}

}