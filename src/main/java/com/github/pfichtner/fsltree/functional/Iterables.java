package com.github.pfichtner.fsltree.functional;

import java.util.ArrayList;
import java.util.List;

public final class Iterables {

	private Iterables() {
		super();
	}

	public static <T> T find(Iterable<T> components, Predicate<T> p) {
		for (T component : components) {
			if (p.apply(component)) {
				return component;
			}
		}
		return null;
	}

	public static <T, S> Iterable<S> transform(Iterable<? extends T> iterable,
			Function<T, S> function) {
		List<S> result = new ArrayList<S>();
		for (T i : iterable) {
			result.add(function.apply(i));
		}
		return result;
	}

}
