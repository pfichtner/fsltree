package com.github.pfichtner.fsltree.visitor;

public interface Visitable {

	void accept(Visitor visitor);

}
