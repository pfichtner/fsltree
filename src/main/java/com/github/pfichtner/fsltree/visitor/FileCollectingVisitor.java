package com.github.pfichtner.fsltree.visitor;

import java.util.ArrayList;
import java.util.List;

import com.github.pfichtner.fsltree.Component;
import com.github.pfichtner.fsltree.File;
import com.github.pfichtner.fsltree.functional.Predicate;
import com.github.pfichtner.fsltree.functional.Predicates;


public class FileCollectingVisitor extends DefaultVisitor {

	private List<File> collectTo;
	private final Predicate<Component> predicate;

	public FileCollectingVisitor() {
		this(Predicates.<Component> alwaysTrue());
	}

	public FileCollectingVisitor(Predicate<Component> predicate) {
		this(new ArrayList<File>(), predicate);
	}

	public FileCollectingVisitor(List<File> collectTo,
			Predicate<Component> predicate) {
		this.collectTo = collectTo;
		this.predicate = predicate;
	}
	
	@Override
	public boolean visit(File file) {
		if (this.predicate.apply(file)) {
			this.collectTo.add(file);
		}
		return true;
	}

	public List<File> getFiles() {
		return this.collectTo;
	}

}
