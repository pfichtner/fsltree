package com.github.pfichtner.fsltree.visitor;

import com.github.pfichtner.fsltree.Directory;
import com.github.pfichtner.fsltree.File;

public interface Visitor {

	boolean visit(Directory subdir);

	boolean visit(File file);

}
