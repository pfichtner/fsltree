package com.github.pfichtner.fsltree.visitor;

import java.util.Stack;

import com.github.pfichtner.fsltree.Component;
import com.github.pfichtner.fsltree.Directory;
import com.github.pfichtner.fsltree.File;

public class DefaultVisitor implements Visitor {

	private final Stack<Directory> directories = new Stack<Directory>();

	@Override
	public boolean visit(Directory subdir) {
		for (Component component : subdir.list()) {
			if (component instanceof Directory) {
				this.directories.push(subdir);
				visit((Directory) component);
				this.directories.pop();
			} else {
				visit((File) component);
			}
		}
		return true;
	}

	@Override
	public boolean visit(File file) {
		// do nothing
		return true;
	}

}
