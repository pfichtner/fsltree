package com.github.pfichtner.fsltree;

import static com.github.pfichtner.fsltree.functional.Predicates.compose;
import static com.github.pfichtner.fsltree.functional.Predicates.contains;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.junit.Test;

import com.github.pfichtner.fsltree.visitor.FileCollectingVisitor;

public class TreeBuilderTest {

	private static final String BASE_DIR = "/home/me/Musik/";

	private final List<Song> library = Collections
			.unmodifiableList(createLibrary());

	@Test
	public void testBuildTree() {

		Directory root = TreeBuilder.createFromPath(Song.getFile).create(
				this.library);

		Directory theRealThing = (Directory) root
				.list("home/me/Musik/EigeneCDs/Faith_No_More/The_Real_Thing"
						.split("\\/"));
		assertEquals(11, theRealThing.list().size());
		assertFilesOnly(theRealThing.list());

		Directory stAnger = (Directory) root
				.list("home/me/Musik/EigeneCDs/Metallica/St._Anger/"
						.split("\\/"));
		assertEquals(11, stAnger.list().size());
		assertFilesOnly(stAnger.list());

		Directory raisingHell = (Directory) root
				.list("home/me/Musik/MP3s/Run_Dmc/Raising_Hell/".split("\\/"));
		assertEquals(12, raisingHell.list().size());
		assertFilesOnly(raisingHell.list());

		FileCollectingVisitor visitor = new FileCollectingVisitor(compose(
				contains("om"), Component.getName));
		root.accept(visitor);
		List<File> matches = visitor.getFiles();
		assertEquals(faithNoMore().get(0), matches.get(0).getContent());
		assertEquals(faithNoMore().get(4), matches.get(1).getContent());
		assertEquals(faithNoMore().get(8), matches.get(2).getContent());
		assertEquals(metallica().get(2), matches.get(3).getContent());
	}

	@Test
	public void testFirstFiles() {
		Directory root = TreeBuilder.createFromPath(Song.getFile).create(
				this.library);
		assertEquals(
				"From Out Of Nowhere",
				((Song) ((File) root
						.list("home/me/Musik/EigeneCDs/Faith_No_More/The_Real_Thing/01_From_Out_Of_Nowhere.mp3"
								.split("\\/"))).getContent()).getTitle());
		assertEquals(
				"Frantic",
				((Song) ((File) root
						.list("home/me/Musik/EigeneCDs/Metallica/St._Anger/01_Frantic.mp3"
								.split("\\/"))).getContent()).getTitle());

	}

	@Test
	public void testLastFiles() {
		Directory root = TreeBuilder.createFromPath(Song.getFile).create(
				this.library);
		assertEquals(
				"Edge Of The World",
				((Song) ((File) root
						.list("home/me/Musik/EigeneCDs/Faith_No_More/The_Real_Thing/11_Edge_Of_The_World.mp3"
								.split("\\/"))).getContent()).getTitle());
		assertEquals(
				"All Within My Hands",
				((Song) ((File) root
						.list("home/me/Musik/EigeneCDs/Metallica/St._Anger/11_All_Within_My_Hands.mp3"
								.split("\\/"))).getContent()).getTitle());

	}

	private void assertFilesOnly(Iterable<Component> iterable) {
		for (Component component : iterable) {
			assertTrue(component + " not instanceof File",
					component instanceof File);
		}
	}

	private List<Song> createLibrary() {
		List<Song> library = new ArrayList<Song>();
		library.addAll(faithNoMore());
		library.addAll(metallica());
		library.addAll(runDmc());
		library.addAll(peterGabriel());
		return library;
	}

	private List<Song> faithNoMore() {
		String artist = "Faith no more";
		String album = "The real thing";
		String dir = BASE_DIR + "EigeneCDs/Faith_No_More/The_Real_Thing/";
		return Arrays.asList(new Song(artist, album, "From Out Of Nowhere", dir
				+ "01_From_Out_Of_Nowhere.mp3"), new Song(artist, album,
				"Epic", dir + "02_Epic.mp3"), new Song(artist, album,
				"Falling To Pieces", dir + "03_Falling_To_Pieces.mp3"),
				new Song(artist, album, "Surprise! You're Dead", dir
						+ "04_Surprise!_Youre_Dead.mp3"), new Song(artist,
						album, "Zombie Eaters", dir + "05_Zombie_Eaters.mp3"),
				new Song(artist, album, "The Real Thing", dir
						+ "06_The_Real_Thing.mp3"), new Song(artist, album,
						"Underwater Love", dir + "07_Underwater_Love.mp3"),
				new Song(artist, album, "The Morning After", dir
						+ "08_The_Morning_After.mp3"), new Song(artist, album,
						"Woodpecker From Mars", dir
								+ "09_Woodpecker_From_Mars.mp3"), new Song(
						artist, album, "War Pigs", dir + "10_War_Pigs.mp3"),
				new Song(artist, album, "Edge Of The World", dir
						+ "11_Edge_Of_The_World.mp3"));
	}

	private List<Song> metallica() {
		String artist = "Metallica";
		String album = "St. Anger";
		String dir = BASE_DIR + "EigeneCDs/Metallica/St._Anger/";
		return Arrays.asList(new Song(artist, album, "Frantic", dir
				+ "01_Frantic.mp3"), new Song(artist, album, "St. Anger", dir
				+ "02_St._Anger.mp3"), new Song(artist, album,
				"Some Kind Of Monster", dir + "03_Some_Kind_Of_Monster.mp3"),
				new Song(artist, album, "Dirty Window", dir
						+ "04_Dirty_Window.mp3"), new Song(artist, album,
						"Invisible Kid", dir + "05_Invisible_Kid.mp3"),
				new Song(artist, album, "My World", dir + "06_My_World.mp3"),
				new Song(artist, album, "Shoot Me Again", dir
						+ "07_Shoot_Me_Again.mp3"), new Song(artist, album,
						"Sweet Amber", dir + "08_Sweet_Amber.mp3"), new Song(
						artist, album, "The Unnamed Feeling", dir
								+ "09_The_Unnamed_Feeling.mp3"), new Song(
						artist, album, "Purify", dir + "10_Purify.mp3"),
				new Song(artist, album, "All Within My Hands", dir
						+ "11_All_Within_My_Hands.mp3"));
	}

	private List<Song> runDmc() {
		String artist = "Run DMC";
		String album = "Raising Hell";
		String dir = BASE_DIR + "MP3s/Run_Dmc/Raising_Hell/";

		return Arrays
				.asList(new Song(artist, album, "Peter Piper", dir
						+ "01-Peter Piper.mp3"), new Song(artist, album,
						"It's Tricky", dir + "02-It's Tricky.mp3"), new Song(
						artist, album, "My Adidas", dir + "03-My Adidas.mp3"),
						new Song(artist, album, "Walk This Way", dir
								+ "04-Walk This Way.mp3"),
						new Song(artist, album, "Is It Live", dir
								+ "05-Is It Live.mp3"), new Song(artist, album,
								"Perfection", dir + "06-Perfection.mp3"),
						new Song(artist, album, "Hit It Run", dir
								+ "07-Hit It Run.mp3"), new Song(artist, album,
								"Raising Hell", dir + "08-Raising Hell.mp3"),
						new Song(artist, album, "You Be Illin'", dir
								+ "09-You Be Illin'.mp3"), new Song(artist,
								album, "Dumb Girl", dir + "10-Dumb Girl.mp3"),
						new Song(artist, album, "Son Of Byford", dir
								+ "11-Son Of Byford.mp3"), new Song(artist,
								album, "Proud To Be Black", dir
										+ "12-Proud To Be Black.mp3"));
	}

	private List<Song> peterGabriel() {
		String artist = "Run DMC";
		String album = "Raising Hell";
		String dir = BASE_DIR + "EigeneCDs/Peter_Gabriel/So/";

		return Arrays.asList(new Song(artist, album, "Red Rain", dir
				+ "01_Red_Rain.mp3"), new Song(artist, album, "Sledgehammer",
				dir + "02_Sledgehammer.mp3"), new Song(artist, album,
				"Don't Give Up", dir + "03_Dont_Give_Up.mp3"), new Song(artist,
				album, "That Voice Again", dir + "04_That_Voice_Again.mp3"),
				new Song(artist, album, "In Your Eyes", dir
						+ "05_In_Your_Eyes.mp3"), new Song(artist, album,
						"Mercy Street", dir + "06_Mercy_Street.mp3"), new Song(
						artist, album, "Big Time", dir + "07_Big_Time.mp3"),
				new Song(artist, album, "We Do What Were Told milgrams 37", dir
						+ "08_We_Do_What_Were_Told_milgrams_37.mp3"), new Song(
						artist, album, "This is the Picture excellent birds",
						dir + "09_This_is_the_Picture_excellent_birds.mp3"));
	}

}
