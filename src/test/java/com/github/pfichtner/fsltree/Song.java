package com.github.pfichtner.fsltree;

import com.github.pfichtner.fsltree.functional.Function;

public class Song {

	public static final Function<Song, String> getFile = new Function<Song, String>() {

		@Override
		public String apply(Song song) {
			return song.getFile();
		}
	};

	private final String artist, album, title, file;

	public Song(String artist, String album, String title, String file) {
		this.artist = artist;
		this.album = album;
		this.title = title;
		this.file = file;
	}

	public String getArtist() {
		return this.artist;
	}

	public String getAlbum() {
		return this.album;
	}

	public String getTitle() {
		return this.title;
	}

	public String getFile() {
		return this.file;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((this.album == null) ? 0 : this.album.hashCode());
		result = prime * result
				+ ((this.artist == null) ? 0 : this.artist.hashCode());
		result = prime * result
				+ ((this.file == null) ? 0 : this.file.hashCode());
		result = prime * result
				+ ((this.title == null) ? 0 : this.title.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Song other = (Song) obj;
		if (this.album == null) {
			if (other.album != null)
				return false;
		} else if (!this.album.equals(other.album))
			return false;
		if (this.artist == null) {
			if (other.artist != null)
				return false;
		} else if (!this.artist.equals(other.artist))
			return false;
		if (this.file == null) {
			if (other.file != null)
				return false;
		} else if (!this.file.equals(other.file))
			return false;
		if (this.title == null) {
			if (other.title != null)
				return false;
		} else if (!this.title.equals(other.title))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Song [artist=" + this.artist + ", album=" + this.album
				+ ", title=" + this.title + ", file=" + this.file + "]";
	}

}
