package com.github.pfichtner.fsltree;

import static com.github.pfichtner.fsltree.Trees.defaultPathSplitter;
import static com.github.pfichtner.fsltree.Trees.newSortedTree;
import static com.github.pfichtner.fsltree.Trees.newTree;
import static com.github.pfichtner.fsltree.functional.Iterables.transform;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.junit.Test;

import com.github.pfichtner.fsltree.visitor.FileCollectingVisitor;

public class TreesTest {

	@Test
	public void testThatPassingAnEmptyListReturnsAnEmptyRootNode() {
		Directory root = newTree(Collections.<String> emptyList());
		assertTrue(root.list().isEmpty());
	}

	@Test
	public void testThatIllegalPathsWithDuplictaPathSeperatorsWillBeIgnored() {
		Directory root = newTree(Arrays.asList("/home/foo//bar.txt"));
		Directory l1 = (Directory) root.list("home", "foo");
		List<Component> list = l1.list();
		assertEquals(1, list.size());
		assertEquals("/home/foo/bar.txt", list.get(0).getAbsolutePath());
	}

	@Test
	public void testNonExisiting() {
		Directory root = newTree(Arrays.asList("/home/foo/bar.txt"));
		assertNull(root.list("home", "foo", "XXXbar.txtXXX"));
		assertNull(root.list("home", "XXXfooXXX"));
		assertNull(root.list("XXXhomeXXX", "bar"));
	}

	@Test
	public void testSorted() {
		Directory root = newSortedTree(Arrays.asList("/home/a/2", "/home/b/2",
				"/home/b/1", "/home/a/3", "/home/a/1"));
		FileCollectingVisitor visitor = new FileCollectingVisitor();
		root.accept(visitor);
		assertEquals(Arrays.asList("/home/a/1", "/home/a/2", "/home/a/3",
				"/home/b/1", "/home/b/2"),
				transform(visitor.getFiles(), Component.getAbsolutePath));
	}

	@Test
	public void testNewTree() {
		List<String> files = Arrays.asList(
				"/etc/initramfs-tools/conf.d/resume",
				"/etc/initramfs-tools/initramfs.conf",
				"/etc/initramfs-tools/scripts/panic",
				"/etc/initramfs-tools/scripts/nfs-bottom",
				"/etc/initramfs-tools/scripts/local-premount",
				"/etc/initramfs-tools/scripts/init-premount",
				"/etc/initramfs-tools/scripts/init-bottom",
				"/etc/initramfs-tools/scripts/local-top",
				"/etc/initramfs-tools/scripts/nfs-top",
				"/etc/initramfs-tools/scripts/nfs-premount",
				"/etc/initramfs-tools/scripts/local-bottom",
				"/etc/initramfs-tools/scripts/init-top",
				"/etc/initramfs-tools/modules",
				"/etc/initramfs-tools/update-initramfs.conf",
				"/etc/initramfs-tools/hooks");

		Directory root = newTree(files, defaultPathSplitter());

		// list all
		{
			FileCollectingVisitor visitor = new FileCollectingVisitor();
			root.accept(visitor);
			assertEquals(files,
					transform(visitor.getFiles(), Component.getAbsolutePath));
		}

		// list /etc/initramfs-tools/scripts
		{
			FileCollectingVisitor visitor = new FileCollectingVisitor();
			root.list(new String[] { "etc", "initramfs-tools", "scripts" })
					.accept(visitor);
			assertEquals(Arrays.asList("/etc/initramfs-tools/scripts/panic",
					"/etc/initramfs-tools/scripts/nfs-bottom",
					"/etc/initramfs-tools/scripts/local-premount",
					"/etc/initramfs-tools/scripts/init-premount",
					"/etc/initramfs-tools/scripts/init-bottom",
					"/etc/initramfs-tools/scripts/local-top",
					"/etc/initramfs-tools/scripts/nfs-top",
					"/etc/initramfs-tools/scripts/nfs-premount",
					"/etc/initramfs-tools/scripts/local-bottom",
					"/etc/initramfs-tools/scripts/init-top"),
					transform(visitor.getFiles(), Component.getAbsolutePath));
		}

	}

}
