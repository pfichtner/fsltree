package com.github.pfichtner.fsltree;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;

import org.junit.Test;

import com.github.pfichtner.fsltree.File;
import com.github.pfichtner.fsltree.impl.DefaultDirectory;
import com.github.pfichtner.fsltree.impl.DefaultFile;
import com.github.pfichtner.fsltree.visitor.FileCollectingVisitor;


public class DirectoryTest {

	@Test
	public void testOneFile() {
		DefaultDirectory root = DefaultDirectory.newRoot();
		DefaultFile f1 = new DefaultFile("foo.txt", root);
		assertEquals(Arrays.asList(f1), root.list());
	}

	@Test
	public void testTwoFiles() {
		DefaultDirectory root = DefaultDirectory.newRoot();
		DefaultFile f1 = new DefaultFile("foo.txt", root);
		DefaultFile f2 = new DefaultFile("bar.txt", root);
		assertEquals(Arrays.asList(f1, f2), root.list());
	}

	@Test
	public void testOneSubDirs() {
		DefaultDirectory root = DefaultDirectory.newRoot();
		File file = new DefaultFile("mirrors", new DefaultDirectory("prefs",
				new DefaultDirectory("mplayer", new DefaultDirectory("lib",
						new DefaultDirectory("var", root)))));
		FileCollectingVisitor visitor = new FileCollectingVisitor();
		root.accept(visitor);
		assertEquals(Arrays.asList(file), visitor.getFiles());
	}

}
