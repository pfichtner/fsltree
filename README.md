# fsltree [![pipeline status](https://gitlab.com/pfichtner/fsltree/badges/master/pipeline.svg)](https://gitlab.com/pfichtner/fsltree/commits/master)
A tree with a filesystem like behavior (filesystem like tree -> fsltree)

## Download
You can download the latest version of [fsltree](https://github.com/pfichtner/fsltree/releases/latest) from github's release page.

## License
Copyright 2013-2014 Peter Fichtner - Released under the [Apache 2.0 license](http://www.apache.org/licenses/LICENSE-2.0.html)
